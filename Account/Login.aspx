﻿<%@ Page Title="Log In" Language="C#"  MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="Account_Login" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha, Version=1.0.5.0, Culture=neutral, PublicKeyToken=9afc4d65b28c38c2" %>
 
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <h3>Вход</h3>
    <table>
      <tr>
        <td>
          Логин :</td>
        <td>
          <asp:TextBox ID="UserName" runat="server" /></td>
        <td>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
            ControlToValidate="UserName"
            Display="Dynamic" 
            ErrorMessage="Не может быть пустым" 
            runat="server" />
        </td>
      </tr>
      <tr>
        <td>
          Пароль :</td>
        <td>
          <asp:TextBox ID="UserPass" TextMode="Password" 
             runat="server" />
        </td>
        <td>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
            ControlToValidate="UserPass"
            ErrorMessage="Не может быть пустым" 
            runat="server" />
        </td>
      </tr>
      <tr>
        <td>
          Запомнить меня?</td>
        <td>
          <asp:CheckBox ID="Persist" runat="server" /></td>
      </tr>
    </table>
    <asp:Button ID="Submit1" OnClick="LogonClick" Text="Войти" 
       runat="server" />
    <recaptcha:RecaptchaControl
    ID="recaptcha"
    runat="server"
    PublicKey="6LeFzdASAAAAAE039hS45h65WYAHxrnIgXo4XMv_"
    PrivateKey="6LeFzdASAAAAANfZoq349CT_Kc01cPv0Ec9TCgVF"
    />       

    <p>
      <asp:Label ID="Msg" ForeColor="red" runat="server" />
    </p>

</asp:Content>