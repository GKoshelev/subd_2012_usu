﻿<%@ Page Title="Profile" Language="C#" Culture="ru-RU" UICulture="ru-RU" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Profile.aspx.cs" Inherits="Account_Profile"  %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager> 
<h3>Мой профиль</h3>
    <table id = "info_edit" style = "display:none" runat = "server">
        <tr>
            <td>Логин :</td>
            <td><asp:Label ID = "Login" runat="server"></asp:Label></td>
        </tr>
        <tr>
        <td>Имя :</td>
        <td>
            <asp:TextBox ID="NameE" TextMode="SingleLine" runat="server" tabindex="1" />
            <asp:RequiredFieldValidator ID="NameEValidator" ControlToValidate="NameE" ErrorMessage="Не может быть пустым" runat="server" />
        </td>
      </tr>
        <tr>
            <td>Фамилия :</td>
            <td>
                <asp:TextBox ID="SurnameE" TextMode="SingleLine" runat="server" tabindex="2" />
                <asp:RequiredFieldValidator ID="SurnameEValidator" ControlToValidate="SurnameE" ErrorMessage="Не может быть пустым" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Отчество :</td>
            <td>
                <asp:TextBox ID="SnameE" TextMode="SingleLine" runat="server" tabindex="3" />
                <asp:RequiredFieldValidator ID="SnameEValidator" ControlToValidate="SnameE" ErrorMessage="Не может быть пустым" runat="server" />
            </td>    
        </tr>
        <tr>
            <td>Паспортные данные:</td>
        </tr>

        <tr>
            <td>Серия:</td>
            <td>
                <asp:TextBox ID="PSer" TextMode="SingleLine" runat="server" MaxLength="4" tabindex="4" />
                <ajaxToolkit:FilteredTextBoxExtender ID="PSerFilter" runat="server" TargetControlID="PSer" ValidChars="1234567890" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="PSer" ErrorMessage="Не может быть пустым" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Номер:</td>
            <td>
                <asp:TextBox ID="PInd" TextMode="SingleLine" runat="server" MaxLength = "6" tabindex="5" /> 
                <ajaxToolkit:FilteredTextBoxExtender ID="PNumFilter" runat="server" TargetControlID="PInd" ValidChars="1234567890" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="PInd" ErrorMessage="Не может быть пустым" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Дата выдачи:</td>
            <td><asp:TextBox ID="PDate" runat="server" tabindex="6"/>
            <ajaxToolkit:CalendarExtender FirstDayOfWeek="Monday" Format="dd.MM.yyyy" ID="C" runat="server" TargetControlID="PDate" /> 
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="PDate" ErrorMessage="Не может быть пустым" runat="server" />
        </td>   
        </tr>
        <tr>
            <td><asp:Label runat="server">Регион :</asp:Label></td>
            <td><asp:TextBox ID="RegionE" runat="server" tabindex="7"></asp:TextBox>
            <ajaxToolkit:AutoCompleteExtender 
            runat="server" 
            ID="AutoCompleteExtender1" 
            BehaviorID="autocomplteextender1"
            TargetControlID="RegionE"
            ServiceMethod="GetRegion"
            ServicePath="/SUBD2012/Search.asmx"
            MinimumPrefixLength="1" 
            CompletionInterval="0"
            EnableCaching="true"
            CompletionSetCount="20" 
            CompletionListCssClass="AutoExtender"
            CompletionListItemCssClass="AutoExtenderList"
            CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
            DelimiterCharacters=";, :"
            OnClientItemSelected="RegionSelected"
            OnClientPopulated="RegionBeforeSelect"
            ShowOnlyCurrentWordInCompletionListItem="true">
            </ajaxToolkit:AutoCompleteExtender>
            <asp:TextBox runat="server" ID="RegionEH" CssClass="pseudoTextBox" onfocus="this.blur()"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td><asp:Label runat="server">Населенный пункт :</asp:Label></td>
            <td><asp:TextBox ID="CityE"  runat="server"  tabindex="8"></asp:TextBox>
            <asp:TextBox runat="server" ID="CityEH" CssClass="pseudoTextBox" onfocus="this.blur()"></asp:TextBox>
            <ajaxToolkit:AutoCompleteExtender 
            runat="server" 
            ID="AutoCompleteExtender2" 
            BehaviorID="autocomplteextender2"
            TargetControlID="CityE"
            ServiceMethod="GetCity"
            ServicePath="/SUBD2012/Search.asmx"
            MinimumPrefixLength="1" 
            CompletionInterval="0"
            EnableCaching="true"
            CompletionSetCount="20" 
            CompletionListCssClass="AutoExtender"
            CompletionListItemCssClass="AutoExtenderList"
            CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
            OnClientItemSelected="CitySelected"
            OnClientPopulated="CityBeforeSelect"
            DelimiterCharacters=";, :"
            ShowOnlyCurrentWordInCompletionListItem="true"/>
            </td>
        </tr>
        <tr>
        <td><asp:Label runat="server">Улица :</asp:Label></td>
        <td><asp:TextBox ID="StreetE"  runat="server"  tabindex="9"></asp:TextBox>
        <asp:TextBox runat="server" ID="StreetEH" CssClass="pseudoTextBox" onfocus="this.blur()"></asp:TextBox>
        <ajaxToolkit:AutoCompleteExtender 
            runat="server" 
            ID="AutoCompleteExtender3" 
            BehaviorID="autocomplteextender3"
            TargetControlID="StreetE"
            ServiceMethod="GetStreet"
            ServicePath="/SUBD2012/Search.asmx"
            MinimumPrefixLength="1" 
            CompletionInterval="0"
            EnableCaching="true"
            CompletionSetCount="20" 
            CompletionListCssClass="AutoExtender"
            CompletionListItemCssClass="AutoExtenderList"
            CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
            OnClientItemSelected="StreetSelected"
            OnClientPopulated="StreetBeforeSelect"
            DelimiterCharacters=";, :"
            ShowOnlyCurrentWordInCompletionListItem="true"/>
        </td>
    </tr>
        <tr>
            <td><asp:Label ID="Label3" runat="server">Дом :</asp:Label></td>
            <td><asp:TextBox ID="HouseE"  runat="server" tabindex="10"></asp:TextBox>
            <ajaxToolkit:AutoCompleteExtender 
                runat="server" 
                ID="AutoCompleteExtender4" 
                BehaviorID="autocomplteextender4"
                TargetControlID="HouseE"
                ServiceMethod="GetHouse"
                ServicePath="/SUBD2012/Search.asmx"
                MinimumPrefixLength="0" 
                CompletionInterval="0"
                EnableCaching="true"
                CompletionSetCount="20" 
                CompletionListCssClass="AutoExtender"
                CompletionListItemCssClass="AutoExtenderList"
                CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                OnClientItemSelected="HouseSelected"
                OnClientPopulated="HouseBeforeSelect"
                DelimiterCharacters=";, :"
                ShowOnlyCurrentWordInCompletionListItem="true"/>
            </td>
        </tr>   
        <tr>
            <td><asp:Label ID="Label6" runat="server">Корпус :</asp:Label></td>
            <td><asp:TextBox ID="CorpE"  runat="server"  tabindex="11"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label5" runat="server">Квартира :</asp:Label></td>
            <td><asp:TextBox ID="FlatE"  runat="server"  tabindex="12"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label2" runat="server">Индекс :</asp:Label></td>
            <td><asp:TextBox ID="IndE"  runat="server" CssClass="pseudoTextBox" onfocus="this.blur()"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Button ID="Confirm1" onclick="AddInfo" Text="Подтвердить" runat="server" CssClass="pseudoButton" tabindex="13"/></td> 
        </tr>
    </table>

    <table id = "backInfo_view" style = "display: none" runat="server" >
          <tr>
            <td><div ID="backButton" class = "pseudoButton"><asp:Label ID="Label4" Text="Отмена изменений" runat="server"/></div></td>
          </tr>
    </table>

    <table id = "info_view" style = "display:none" runat = "server">
        <tr>
            <td>Логин :</td>
            <td><asp:Label ID = "LoginV" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Имя :</td>
            <td><asp:Label ID = "Name" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Фамилия :</td>
            <td><asp:Label ID = "Surname" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Отчество :</td>
            <td><asp:Label ID = "Sname" runat="server"></asp:Label></td>
        </tr>  
        <tr>
            <td>Паспортные данные :</td>
        </tr>  
        <tr>
            <td><asp:Label ID = "PSerV" runat="server" ></asp:Label></td>
            <td><asp:Label ID = "PIndV" runat="server"></asp:Label></td>
            <td><asp:Label ID = "PDateV" runat="server"></asp:Label></td>
        </tr>  
        <tr>
            <td><asp:Label ID="RegionL" runat="server">Регион :</asp:Label></td>
            <td><asp:Label ID="RegionT" runat="server"></asp:Label></td>
            <td><asp:Label ID="RegionH" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td><asp:Label ID="CityL" runat="server">Населенный пункт :</asp:Label></td>
            <td><asp:Label ID="CityT"  runat="server"></asp:Label></td>
            <td><asp:Label ID="CityH"  runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td><asp:Label ID="StreetL" runat="server">Улица :</asp:Label></td>
            <td><asp:Label ID="StreetT"  runat="server" ></asp:Label></td>
            <td><asp:Label ID="StreetH"  runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><asp:Label ID="HouseL" runat="server">Дом :</asp:Label></td>
            <td><asp:Label ID="HouseT"  runat="server"></asp:Label></td>
            <td><asp:Label ID="CorpT"  runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td><asp:Label ID="FlatL" runat="server">Квартира :</asp:Label></td>
            <td><asp:Label ID="FlatT"  runat="server"></asp:Label></td>
        </tr>

        <tr>
            <td><asp:Label ID="IndexL" runat="server">Индекс :</asp:Label></td>
            <td><asp:Label ID="IndexT"  runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><div ID="editButton" class = "pseudoButton"><asp:Label ID="Label1" Text="Редактировать" runat="server"/></div></td>
        </tr>
    </table>
    <p>
      <asp:Label ID="Msg" ForeColor="red" runat="server" />
    </p>
</asp:Content>