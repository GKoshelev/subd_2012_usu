﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Security;

public partial class Account_Login : System.Web.UI.Page
{
    protected void LogonClick ( object sender , EventArgs e )
    {
        if (Page.IsValid)
        {
            var login = UserName.Text;
            var pass = UserPass.Text;
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["KladrService"].ConnectionString);
            conn.Open();
            var check =
                new SqlCommand(
                    "select id from George.accounts where user_login = '" + login + "' and user_password = '" + pass +
                    "'", conn);
            SqlDataReader sdr = check.ExecuteReader();
            if (sdr.HasRows)
            {
                sdr.Read();
                var id = (int) sdr.GetValue(0);
                Session.Add("login", login);
                Session.Add("id", id);
                Session.Timeout = 30;
                conn.Close();
                FormsAuthentication.RedirectFromLoginPage(UserName.Text, Persist.Checked);
            }
            else
            {
                conn.Close();
                Msg.Text = "Что-то не так с введенными данными.";
            }
        }
        else
            Msg.Text = "Неправильная капча.";            
    }
}
