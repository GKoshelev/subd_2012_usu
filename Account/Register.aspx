﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Account_Register" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha, Version=1.0.5.0, Culture=neutral, PublicKeyToken=9afc4d65b28c38c2" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h3>Регистрация</h3>
    <table>
      <tr>
        <td>
          Логин :</td>
        <td>
          <asp:TextBox ID="UserName" runat="server" /></td>
        <td>
          <asp:RequiredFieldValidator ID="UserNameValidator" 
            ControlToValidate="UserName"
            ErrorMessage="Не может быть пустым" 
            runat="server" />
        </td>
      </tr>
      <tr>
        <td>
          Пароль :</td>
        <td>
          <asp:TextBox ID="Password" TextMode="Password" 
             runat="server" />
        </td>
        <td>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
            ControlToValidate="Password"
            ErrorMessage="Не может быть пустым" 
            runat="server" />
        </td>
      </tr>      
      <tr>
        <td>
          Повторить пароль :</td>
        <td>
          <asp:TextBox ID="ConfirmPassword" TextMode="Password" 
             runat="server" />
        </td>
        <td>
            <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" 
            ErrorMessage="Пароль и повторный пароли не совпадают."/>
        </td>
      </tr>
    </table>
    <asp:Button ID="Submit1" OnClick="RegisterClick" Text="Войти" 
       runat="server" />
    <recaptcha:RecaptchaControl
    ID="recaptcha"
    runat="server"
    PublicKey="6LeFzdASAAAAAE039hS45h65WYAHxrnIgXo4XMv_"
    PrivateKey="6LeFzdASAAAAANfZoq349CT_Kc01cPv0Ec9TCgVF"
    />  
    <p>
      <asp:Label ID="Msg" ForeColor="red" runat="server" />
    </p>
</asp:Content>