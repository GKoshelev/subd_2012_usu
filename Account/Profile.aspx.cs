﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;

public partial class Account_Profile : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        var login = Session["login"];
        if (login == null)
            Response.Redirect("~/Account/Login.aspx");

        var action = Request.QueryString["Action"];
        if (action == "show")
            ShowInfo();
        else
            Response.Redirect("~/Default.aspx");   
    }
    protected string GetLogin()
    {
        var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
        if (authCookie != null)
        {
                var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                return ticket.Name;
        }
        return "Something went bad =(";
    }
    protected int GetId(SqlConnection conn, string login)
    {
        var query = new SqlCommand("select id from George.accounts where user_login = '" + login + "'" , conn);
        return (int) query.ExecuteScalar();
    }
    protected string GetRegion(string code , SqlConnection conn)
    {
        var query = new SqlCommand("select name , socr from George.regions where code like '" + code + "'", conn);
        using(SqlDataReader reader = query.ExecuteReader())
        {
            var str = "";
            if (reader.Read())
                str = reader.GetString(1) + " " + reader.GetString(0);
            reader.Close();
            return str;
        }
    }
    protected string GetCity ( string code , SqlConnection conn )
    {
        var query = new SqlCommand("select * from George.cities where code like '" + code + "'",conn);
        using(SqlDataReader reader = query.ExecuteReader())
        {
            var str = "";
            if(reader.Read())
            {
                var prn = reader.GetString(3);
                if (prn != "")
                    str = reader.GetString(4) + prn;
                var pcn = reader.GetString(5);
                if (pcn != "")
                    str += reader.GetString(6) + pcn;
                str = reader.GetString(1) + " " + reader.GetString(0);                
                reader.Close();
            }
            return str;
        }
    }
    protected string GetStreet ( string code , SqlConnection conn )
    {
        var query = new SqlCommand("select name , socr from street where code like '" + code + "'",conn);
        using (SqlDataReader reader = query.ExecuteReader())
        {
            var str = "";
            if (reader.Read())
                str = reader.GetString(1) + " " + reader.GetString(0);
            reader.Close();
            return str;
        }
    }
    protected void ShowInfo()
    {
        using
        (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["KladrService"].ConnectionString))
        {
            conn.Open();
            var login = Session["login"].ToString();
            var id = Session["id"].ToString();
            var query = new SqlCommand("select name , surname , sname , Pser , PInd , PDate from George.fio where id = '"+id+"'", conn);
            using (SqlDataReader reader = query.ExecuteReader())
            {
                if(reader.Read())
                {
                    LoginV.Text = login;
                    info_view.Attributes.Add("style" , "display : block;");
                    Name.Text = reader.GetValue(0).ToString();
                    Surname.Text = reader.GetValue(1).ToString();
                    Sname.Text = reader.GetValue(2).ToString();
                    PSerV.Text = reader.GetValue(3).ToString();
                    PIndV.Text = reader.GetValue(4).ToString();
                    PDateV.Text = reader.GetValue(5).ToString();
                    reader.Close();
                    var query1 = new SqlCommand("select * from George.adresses where id = '"+id+"'",conn);
                    using (SqlDataReader reader1 = query1.ExecuteReader())
                    {
                        reader1.Read();
                        RegionT.Text = reader1.GetValue(1).ToString();
                        RegionH.Text = reader1.GetValue(2).ToString();
                        CityT.Text = reader1.GetValue(3).ToString();
                        CityH.Text = reader1.GetValue(4).ToString();
                        StreetT.Text = reader1.GetValue(5).ToString();
                        StreetH.Text = reader1.GetValue(6).ToString();
                        HouseT.Text = reader1.GetValue(7).ToString();
                        CorpT.Text = reader1.GetValue(8).ToString();
                        FlatT.Text = reader1.GetValue(9).ToString();
                        IndexT.Text = reader1.GetValue(10).ToString();
                        reader1.Close();
                        var query2 = new SqlCommand("select * from George.codes where id = '"+id+"'",conn);
                        using (SqlDataReader reader2 = query2.ExecuteReader())
                        {
                            reader2.Read();
                            AutoCompleteExtender2.ContextKey = reader2.GetValue(1).ToString();
                            AutoCompleteExtender3.ContextKey = reader2.GetValue(2).ToString();
                            AutoCompleteExtender4.ContextKey = reader2.GetValue(3).ToString();
                            reader2.Close();
                        }
                    }
                }
                else
                {
                    Login.Text = login;
                    info_edit.Attributes.Add("style","display : block;");
                    RegionE.Text = "Свердловская";
                    RegionEH.Text = "обл";
                    CityE.Text = "Екатеринбург";
                    CityEH.Text = "г";
                    CityE.Enabled = true;
                    StreetE.Style.Add("disabled","true");
                    AutoCompleteExtender3.ContextKey = "6600000100000";
                    AutoCompleteExtender2.ContextKey = "66";
                }
                reader.Close();
            }
            conn.Close();
        }
    }

    protected void AddInfo(object sender , EventArgs e)
    {
        var id = Session["id"].ToString();
        var name = NameE.Text;
        var surname = SurnameE.Text;
        var sname = SnameE.Text;
        var pSer = PSer.Text;
        var pInd = PInd.Text;
        var pDate = PDate.Text;
        var regName = RegionE.Text;
        var regHelp = RegionEH.Text;
        var cityName = CityE.Text;
        var cityHelp = CityEH.Text;
        var streetName = StreetE.Text;
        var streetHelp = StreetEH.Text;
        var house = HouseE.Text;
        var corp = CorpE.Text;
        var flat = FlatE.Text;
        var postIndex = IndE.Text;
        var cityCode = AutoCompleteExtender2.ContextKey;
        var streetCode = AutoCompleteExtender3.ContextKey;
        var houseCode = AutoCompleteExtender4.ContextKey;

        var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["KladrService"].ConnectionString);
        conn.Open();

        var check1 = new SqlCommand("select count(*) from George.fio where PSer like '" + pSer + "' and PInd like '" + pInd + "' and id not like '" + id + "'" , conn);
        var count = (int) check1.ExecuteScalar();
        if(count != 0)
        {
            Msg.Text = "Такие данные паспорта уже есть в базе";
            return;
        }
        var check = new SqlCommand("select count(*) from George.fio where id = '" + id + "'" , conn);
        count = (int)check.ExecuteScalar();
        info_edit.Attributes.Add("style" , "display : none;");
        info_view.Attributes.Add("style" , "display : block;");
        if (count == 0)
        {
            var insert =
                new SqlCommand("insert into George.fio (id,name,surname,sname,PSer,PInd,PDate) values ('" + id + "','" + name + "','" + surname + "','" + sname +"','"+pSer + "','" + pInd + "','" + pDate + "')" , conn);
            insert.ExecuteNonQuery();
            var insert1 = new SqlCommand("insert into George.codes (id,city,street,house) values ('"+id+"','"+cityCode+"','"+streetCode+"','"+houseCode+"')" , conn);
            insert1.ExecuteNonQuery();
            var insert2  = new SqlCommand("insert into George.adresses (id,region_name,region_help,city_name,city_help,street_name,street_help,house,corp,flat,post_index) values ('" +
                id + "','" + regName + "','" + regHelp + "','" + cityName + "','" + cityHelp + "','" + streetName +
                "','" + streetHelp + "','" + house + "','"+corp +"','"+ flat + "','" + postIndex + "')", conn);
            insert2.ExecuteNonQuery();
        }
        else
        {
            var update =
                new SqlCommand(
                    "update George.fio set name = '" + name + "',surname  = '" + surname + "',sname='" + sname +"',PSer='"+pSer+"',PInd='"+pInd+"',PDate='"+pDate +"' where id = '" + id + "'" , conn);
            update.ExecuteNonQuery();
            var update1 = new SqlCommand("update George.codes set city = '"+cityCode+"', street = '"+streetCode+"', house = '"+houseCode+"'" , conn);
            update1.ExecuteNonQuery();
            var update2 = new SqlCommand("update George.adresses set region_name = '" + regName + "', region_help = '" +
                                   regHelp + "', city_name = '" + cityName + "',city_help = '" + cityHelp +
                                   "', street_name = '" + streetName + "',street_help = '" + streetHelp + "', house = '" +
                                   house + "', corp = '"+corp +"', flat = '" + flat + "', post_index = '" + postIndex + "' where id = '" + id +
                                   "'" , conn);
            update2.ExecuteNonQuery();
        }
        conn.Close();
        Login.Text = Session["login"].ToString();
        Name.Text = name;
        Surname.Text = surname;
        Sname.Text = sname;
        PSerV.Text = pSer;
        PIndV.Text = pInd;
        PDateV.Text = pDate;
        RegionT.Text = regName;
        RegionH.Text = regHelp;
        CityT.Text = cityName;
        CityH.Text = cityHelp;
        StreetT.Text = streetName;
        StreetH.Text = streetHelp;
        HouseT.Text = house;
        CorpT.Text = corp;
        FlatT.Text = flat;
        IndexT.Text = postIndex;
    }
}