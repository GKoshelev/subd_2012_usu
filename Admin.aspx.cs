﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using AjaxControlToolkit.HTMLEditor;

public partial class Scripts_Admin : System.Web.UI.Page
{
    protected void MakeReport ( object sender , EventArgs e )
    {

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        var login = Session["login"];
        if (login == null)
            Response.Redirect("~/Account/Login.aspx");

        var id = Session["id"].ToString();
        UserList.Items.Clear();
        var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["KladrService"].ConnectionString);
        conn.Open();
        var query = new SqlCommand("select is_superuser from George.accounts where id = '" + id + "'", conn);
        using (SqlDataReader reader = query.ExecuteReader())
        {
            if (reader.Read())
            {
                var allow = reader.GetValue(0).ToString();
                if (allow == "False")
                    Response.Redirect("Default.aspx");
                reader.Close();
                query =
                new SqlCommand(
                    "select * from George.adresses as a cross join George.fio as b cross join George.accounts as c where a.id = b.id and b.id = c.id and c.is_superuser = '0'",
                    conn);
                using (SqlDataReader reader1 =  query.ExecuteReader())
                {
                    while(reader1.Read())
                    {
                        id = reader1.GetValue(0).ToString();
                        var tmp = reader1.GetValue(6).ToString().Split(',');
                        var addr = tmp[0] +" "+ reader1.GetValue(5) + " дом " + reader1.GetValue(7);
                        var flat = reader1.GetValue(8).ToString();
                        if(flat.Length != 0)
                            addr += " кв " + flat + " ";
                        if(tmp.Length>1)
                            addr += tmp[1];
                        var city = reader1.GetValue(3).ToString();
                        if(city.Length !=0)
                        {
                            tmp = reader1.GetValue(4).ToString().Split(',');
                            addr += tmp[0] + city;
                            for (int i = tmp.Length-1; i>0;i--)
                                addr += tmp[i];
                        }
                        if (reader1.GetValue(2).ToString() == "г")
                            addr +=" "+ reader1.GetValue(2) + "  " + reader1.GetValue(1);
                        else
                            addr += " " + reader1.GetValue(1) + "  " + reader1.GetValue(2);
                        var info = reader1.GetValue(19) + " , " + reader1.GetValue(13) + " " + reader1.GetValue(12) +
                                   " " + reader1.GetValue(14) + " , " + addr + " , " + reader1.GetValue(9);
                        var cb = new ListItem {Text = info, Value = reader1.GetValue(0).ToString()};
                        UserList.Items.Add(cb);
                    }

                }
            }
            else
                Response.Redirect("Default.aspx");
        }
    }
}

namespace MyControls
{
    public class CustomEditor : Editor
    {

        protected override void FillTopToolbar()
        {
            TopToolbar.Buttons.Add(new AjaxControlToolkit.HTMLEditor.ToolbarButton.Bold());
            TopToolbar.Buttons.Add(new AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic());
            TopToolbar.Buttons.Add(new AjaxControlToolkit.HTMLEditor.ToolbarButton.Underline());
            TopToolbar.Buttons.Add(new AjaxControlToolkit.HTMLEditor.ToolbarButton.Cut());
            TopToolbar.Buttons.Add(new AjaxControlToolkit.HTMLEditor.ToolbarButton.Copy());
            TopToolbar.Buttons.Add(new AjaxControlToolkit.HTMLEditor.ToolbarButton.PasteText());
        }

        protected override void FillBottomToolbar()
        {
            BottomToolbar.Buttons.Add(new AjaxControlToolkit.HTMLEditor.ToolbarButton.DesignMode());
            BottomToolbar.Buttons.Add(new AjaxControlToolkit.HTMLEditor.ToolbarButton.PreviewMode());
        }

    }
}