﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="About.aspx.cs" Inherits="About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        О проекте
    </h2>
    <p>
        Автор: Кошелев Георгий, КБ-301 <br/>
        Контактный e-mail: george.koshelev@gmail.com</br>

	    © ФГАОУ ВПО «УрФУ имени первого Президента России Б.Н.Ельцина»
	    Контакт-центр: 8-800-100-50-44
    </p>
</asp:Content>
