﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="test.aspx.cs" Inherits="test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
              "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Untitled Page</title>
<link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
<script href = "~/Scripts/jshashset.js"  type="text/javascript"></script>
<script href = "~/Scripts/jshashtable-2.1_src.js"  type="text/javascript"></script>


</head>

<script type="text/javascript">

    var regionhash;
    var cityhash;

    function pageLoad() {
        $find('autocomplteextender1')._onMethodComplete = function (result, context) {
            $find('autocomplteextender1')._update(context, result, true);
            webservice_callback1(result, context);
        };

        $find('autocomplteextender2')._onMethodComplete = function (result, context) {
            $find('autocomplteextender2')._update(context, result, true);
            webservice_callback2(result, context);
        };

    }

    function RegionSelected() {
        document.getElementById("TextBox2").style.display = "block";
        var obj = document.getElementById("TextBox1");
        var key = obj.value;
        var arr = key.split("(", 2);
        obj.value = arr[0];
        document.getElementById("Label1").innerHTML = regionhash[key];
        $find('autocomplteextender2').set_contextKey(regionhash[key]);
    }

    function RegionBeforeSelect() {
        regionhash = {};
        var behavior = $find('autocomplteextender1');
        var target = behavior.get_completionList();
        if (behavior._currentPrefix != null) {
            var i;
            for (i = 0; i < target.childNodes.length; i++) {
                var sValue = target.childNodes[i].innerHTML;
                var arr = sValue.toString().split(":", 2);
                regionhash[arr[0]] = arr[1];
                target.childNodes[i].innerHTML = arr[0];
            }
        }   
    }

    function CityBeforeSelect() {
        cityhash = { };
        var behavior = $find('autocomplteextender2');
        var target = behavior.get_completionList();
        if (behavior._currentPrefix != null) {
            var i;
            for (i = 0; i < target.childNodes.length; i++) {
                var sValue = target.childNodes[i].innerHTML;
                var arr = sValue.toString().split(":", 2);
                var params = arr[0].toString().split(",", 6);
                var showstr = params[1] + " ( " + params[0];
                if(params[2] != null)
                    showstr += " " + params[2] + " " + params[3];
                if(params[4] != null)
                    showstr += " " + params[4] + " " + params[5];
                showstr += " )";
                target.childNodes[i].innerHTML = showstr;
                cityhash[showstr] = arr[1];
            }
        }
    }
    function CitySelected() {
        //document.getElementById("TextBox2").style.display = "block";
        var obj = document.getElementById("TextBox2");
        var key = obj.value;
        var arr = key.split(" (", 2);
        obj.value = arr[0];
        document.getElementById("Label2").innerHTML = cityhash[key];
        $find('autocomplteextender2').set_contextKey(cityhash[key]);
    }


    function webservice_callback1(result, context) {
        if (result == "")
            $find("autocomplteextender1").get_element().style.backgroundColor = "red";
        else
            $find("autocomplteextender1").get_element().style.backgroundColor = "white";
    } 
    function webservice_callback2(result, context) {
        if (result == "")
            $find("autocomplteextender2").get_element().style.backgroundColor = "red";
        else
            $find("autocomplteextender2").get_element().style.backgroundColor = "white";
    }

</script>




<body>
<form id="form1" runat="server">
<div>
</div>



<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">  
</ajaxToolkit:ToolkitScriptManager> 

<table runat="server">
<tr>
    <td>
        Регион :
    </td>
    <td>
<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    <ajaxToolkit:AutoCompleteExtender 
        runat="server" 
        ID="AutoCompleteExtender1" 
        BehaviorID="autocomplteextender1"
        TargetControlID="TextBox1"
        ServiceMethod="GetRegion"
        ServicePath="Search.asmx"
        MinimumPrefixLength="1" 
        CompletionInterval="0"
        EnableCaching="true"
        CompletionSetCount="20" 
        CompletionListCssClass="AutoExtender"
        CompletionListItemCssClass="AutoExtenderList"
        CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
        DelimiterCharacters=";, :"
        OnClientItemSelected="RegionSelected"
        OnClientPopulated="RegionBeforeSelect"
        ShowOnlyCurrentWordInCompletionListItem="true">
     </ajaxToolkit:AutoCompleteExtender>
        </td> 
</tr>
<tr>
    <td>
        Город :
    </td>
    <td>
<asp:TextBox ID="TextBox2"  runat="server" style="display: none;"></asp:TextBox>
<asp:Label ID="Label2" runat="server"></asp:Label>
    <ajaxToolkit:AutoCompleteExtender 
        runat="server" 
        ID="AutoCompleteExtender2" 
        BehaviorID="autocomplteextender2"
        TargetControlID="TextBox2"
        ServiceMethod="GetCity"
        ServicePath="Search.asmx"
        MinimumPrefixLength="1" 
        CompletionInterval="0"
        EnableCaching="true"
        CompletionSetCount="20" 
        CompletionListCssClass="AutoExtender"
        CompletionListItemCssClass="AutoExtenderList"
        CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
        OnClientItemSelected="CitySelected"
        OnClientPopulated="CityBeforeSelect"
        DelimiterCharacters=";, :"
        ShowOnlyCurrentWordInCompletionListItem="true"/>
        </td>
        </tr>
        </table>
</form>
</body>
</html>