﻿using System;

public partial class About : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var login = Session["login"];
        if (login == null)
            Response.Redirect("~/Account/Login.aspx");
    }
}
