﻿var regionhash;
var cityhash;
var streethash;
var indexhash;
var househash;

// ReSharper disable UseOfImplicitGlobalInFunctionScope

function pageLoad() {
    $find('autocomplteextender1')._onMethodComplete = function (result, context) {
        $find('autocomplteextender1')._update(context, result, true);
        webservice_callback1(result, context);
    };

    $find('autocomplteextender2')._onMethodComplete = function (result, context) {
        $find('autocomplteextender2')._update(context, result, true);
        webservice_callback2(result, context);
    };
    $find('autocomplteextender3')._onMethodComplete = function (result, context) {
        $find('autocomplteextender3')._update(context, result, true);
        webservice_callback3(result, context);
    };

}

function RegionSelected() {
    document.getElementById("MainContent_CityE").disabled = false;
    document.getElementById("MainContent_StreetE").disabled = false;
    var key = document.getElementById("MainContent_RegionE").value;
    var arr = key.replace(/[()]/g, '').split(" ");
    var helpMsg = arr[0];
    for (var i = 1; i < arr.length-1; i++)
        helpMsg += " " + arr[i];
    document.getElementById("MainContent_RegionE").value = helpMsg;
    document.getElementById("MainContent_RegionEH").value = arr[arr.length - 1];
    if (arr[arr.length - 1] == "г")
        document.getElementById("MainContent_CityE").disabled = true;
    $find('autocomplteextender2').set_contextKey(regionhash[key]);
    $find('autocomplteextender3').set_contextKey(regionhash[key]);
}

function RegionBeforeSelect() {
    document.getElementById("MainContent_RegionEH").value = "";
    document.getElementById("MainContent_CityE").value = "";
    document.getElementById("MainContent_CityE").disabled = true;
    document.getElementById("MainContent_CityEH").value = "";
    document.getElementById("MainContent_StreetE").disabled = true;
    document.getElementById("MainContent_StreetE").value = "";
    document.getElementById("MainContent_StreetEH").value = "";
    document.getElementById("MainContent_HouseE").disabled = true;
    document.getElementById("MainContent_HouseE").value = "";
    document.getElementById("MainContent_CorpE").value = "";
    document.getElementById("MainContent_CorpE").disabled = true;
    document.getElementById("MainContent_FlatE").value = "";
    document.getElementById("MainContent_FlatE").disabled = true;
    document.getElementById("MainContent_IndE").value = "";
    regionhash = {};
    var behavior = $find('autocomplteextender1');
    var target = behavior.get_completionList();
    if (behavior._currentPrefix != null) {
        var i;
        for (i = 0; i < target.childNodes.length; i++) {
            var arr = target.childNodes[i].innerHTML.toString().split(":", 2);
            regionhash[arr[0]] = arr[1];
            target.childNodes[i].innerHTML = arr[0];
        }
    }
}

function CitySelected() {
    document.getElementById("MainContent_StreetE").disabled = false;
    var key = document.getElementById("MainContent_CityE").value;
    var arr = key.split(" (", 2);
    document.getElementById("MainContent_CityE").value = arr[0].trim();
    var words = arr[1].replace(/[)]/g, '').split(" ", 6);
    var helpMsg = words[1];
    if (words[3] != undefined)
        helpMsg += " , " + words[2] + " " + words[3];
    if (words[5] != undefined)
        helpMsg += " , " + words[4] + " " + words[5];
    document.getElementById("MainContent_CityEH").value = helpMsg;

    $.post("/SUBD2012/Search.asmx/GetCountStreets", { code: cityhash[key] },
   function (data) {
       if ($(data).find("int").text() == 0) {
           document.getElementById("MainContent_StreetE").disabled = true;
           document.getElementById("MainContent_HouseE").disabled = false;
           document.getElementById("MainContent_CorpE").disabled = false;
           document.getElementById("MainContent_FlatE").disabled = false;
       }
   });



    $find('autocomplteextender3').set_contextKey(cityhash[key]);
}


function CityBeforeSelect() {
    cityhash = {};
    document.getElementById("MainContent_CityEH").value = "";
    document.getElementById("MainContent_StreetE").disabled = true;
    document.getElementById("MainContent_StreetE").value = "";
    document.getElementById("MainContent_StreetEH").value = "";
    document.getElementById("MainContent_HouseE").disabled = true;
    document.getElementById("MainContent_HouseE").value = "";
    document.getElementById("MainContent_CorpE").value = "";
    document.getElementById("MainContent_CorpE").disabled = true;
    document.getElementById("MainContent_FlatE").value = "";
    document.getElementById("MainContent_FlatE").disabled = true;
    document.getElementById("MainContent_IndE").value = "";
    var behavior = $find('autocomplteextender2');
    var target = behavior.get_completionList();
    if (behavior._currentPrefix != null) {
        var i;
        for (i = 0; i < target.childNodes.length; i++) {
            var arr = target.childNodes[i].innerHTML.toString().split(":", 2);
            var params = arr[0].toString().split(",", 6);
            var showstr = params[1] + " ( " + params[0];
            if (params[2] != null)
                showstr += " " + params[2] + " " + params[3];
            if (params[4] != null)
                showstr += " " + params[4] + " " + params[5];
            showstr += " )";
            target.childNodes[i].innerHTML = showstr;
            cityhash[showstr] = arr[1];
        }
    }
}

function StreetSelected() {
    document.getElementById("MainContent_HouseE").disabled = false;
    document.getElementById("MainContent_CorpE").disabled = false;
    document.getElementById("MainContent_FlatE").disabled = false;
    var key = document.getElementById("MainContent_StreetE").value;
    if (indexhash[key] != "") {
        document.getElementById("MainContent_IndE").value = indexhash[key];
        $find('autocomplteextender4').set_contextKey('none');
    }
    else
        $find('autocomplteextender4').set_contextKey(streethash[key]);
    var arr = key.split("(", 2);
    var words = arr[0].split(" ");
    var helpMsg = words[words.length - 1];
    if (arr[1] != undefined) {
        var hwords = arr[1].replace(/[)]/g, '').trim().split(" ");
        helpMsg += " , " + hwords[0];
        for (var i = 1; i < hwords.length; i++)
            helpMsg += " " + hwords[i];
    }
    document.getElementById("MainContent_StreetEH").value = helpMsg;
    helpMsg = words[0];
    for (i = 1; i < words.length - 1; i++)
        helpMsg += " " + words[i];
    document.getElementById("MainContent_StreetE").value = helpMsg;
}

function StreetBeforeSelect() {
    document.getElementById("MainContent_StreetEH").value = "";
    document.getElementById("MainContent_HouseE").disabled = true;
    document.getElementById("MainContent_HouseE").value = "";
    document.getElementById("MainContent_CorpE").value = "";
    document.getElementById("MainContent_CorpE").disabled = true;
    document.getElementById("MainContent_FlatE").value = "";
    document.getElementById("MainContent_FlatE").disabled = true;
    document.getElementById("MainContent_IndE").value = "";
    streethash = {};
    indexhash = {};
    var behavior = $find('autocomplteextender3');
    var target = behavior.get_completionList();
    if (behavior._currentPrefix != null) {
        var i;
        for (i = 0; i < target.childNodes.length; i++) {            
            var sValue = target.childNodes[i].innerHTML;
            var arr = sValue.toString().split(":", 2);
            var params = arr[0].toString().split(",", 5);
            var showStr = params[0] + " " + params[1];
            if(params.length!=3)
                showStr += "( " + params[3] + " " + params[4] + " )";
            streethash[showStr] = params[2];
            indexhash[showStr] = arr[1];
            target.childNodes[i].innerHTML = showStr;
        }
    }
}

function HouseSelected() {
    var key = document.getElementById("MainContent_HouseE").value;
    document.getElementById("MainContent_IndE").value = househash[key];
}

function HouseBeforeSelect() {
    document.getElementById("MainContent_CorpE").value = "";
    document.getElementById("MainContent_FlatE").value = "";
    document.getElementById("MainContent_IndE").value = "";
    househash = {};
    var behavior = $find('autocomplteextender4');
    var target = behavior.get_completionList();
    if (behavior._currentPrefix != null) {
        var i;
        for (i = 0; i < target.childNodes.length; i++) {
            var sValue = target.childNodes[i].innerHTML;
            var arr = sValue.toString().split(":", 2);
            househash[arr[0]] = arr[1];
            target.childNodes[i].innerHTML = arr[0];
        }
    }
}

function webservice_callback1(result) {
    if (result == "")
        $find("autocomplteextender1").get_element().style.backgroundColor = "red";
    else
           $find("autocomplteextender1").get_element().style.backgroundColor = "white";
}
function webservice_callback2(result) {
    if (result == "")
        $find("autocomplteextender2").get_element().style.backgroundColor = "red";
    else
        $find("autocomplteextender2").get_element().style.backgroundColor = "white";
}
function webservice_callback3(result) {
    if (result == "")
        $find("autocomplteextender3").get_element().style.backgroundColor = "red";
    else
        $find("autocomplteextender3").get_element().style.backgroundColor = "white";
}
// ReSharper restore UseOfImplicitGlobalInFunctionScope