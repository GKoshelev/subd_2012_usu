﻿$(document).ready(function () {
    $("#MainContent_SurnameE").keypress(function (event) {
        var code = event.charCode;
        var length = this.value.length;
        if ((length == 0 || this.value.charAt(this.value.length - 1) == '-') && code <= 1071 && code >= 1040)
            return true;
        else if ((length != 0 && this.value.charAt(this.value.length - 1) != '-') && code >= 1072 && code <= 1103)
            return true;
        else if (code == '45' && this.value.indexOf('-') == -1 && length != 0)
            return true;
        return false;
    });

    $("#MainContent_NameE").keypress(function (event) {
        var code = event.charCode;
        var length = this.value.length;
        if (length == 0 && code <= 1071 && code >= 1040)
            return true;
        else if (length != 0 && code >= 1072 && code <= 1103)
            return true;
        return false;
    });
    $("#MainContent_SnameE").keypress(function (event) {
        var code = event.charCode;
        var length = this.value.length;
        if (length == 0 && code <= 1071 && code >= 1040)
            return true;
        else if (length != 0 && code >= 1072 && code <= 1103)
            return true;
        return false;
    });


    $("#editButton").click(function () {
        copyInfo();
        document.getElementById("MainContent_info_view").style.display = "none";
        document.getElementById("MainContent_info_edit").style.display = "block";
        document.getElementById("MainContent_backInfo_view").style.display = "block";
        document.getElementById("MainContent_HouseE").disabled = false;
        document.getElementById("MainContent_CorpE").disabled = false;
        document.getElementById("MainContent_FlatE").disabled = false;

    });

    $("#backButton").click(function () {
        document.getElementById("MainContent_info_view").style.display = "block";
        document.getElementById("MainContent_info_edit").style.display = "none";
        document.getElementById("MainContent_backInfo_view").style.display = "none";
    });

    $("#all").click(function () {
        var labId = "MainContent_UserList_";
        var counter = 0;
        while (document.getElementById(labId + counter)) {
            var elem = document.getElementById(labId + counter++);
            if (!elem.checked)
                elem.checked = true;
        }
    });

    $("#no").click(function () {
        var labId = "MainContent_UserList_";
        var counter = 0;
        while (document.getElementById(labId + counter)) {
            var elem = document.getElementById(labId + counter++);
            if (elem.checked)
                elem.checked = false;
        }
    });

    $("#btnSubmit").click(function () {
        var message = document.getElementById("MainContent_Editor1_ctl02_ctl00").contentWindow.document.body.innerHTML;
        var openWindow = window.open("", "newwin", "height=" + $(window).height() + ", width=" + $(window).width() + ",toolbar=no,scrollbars=" + scroll + ",menubar=no");
        openWindow.content = "";
        openWindow.document.write("<TITLE>Title Goes Here</TITLE><BODY><h1>Письма студентам:</h1>");
        var counter = 0;
        while (document.getElementById("MainContent_UserList_" + counter)) {
            var elem = document.getElementById("MainContent_UserList_" + counter);
            var label = document.getElementById("MainContent_UserList_" + counter++).nextSibling;
            if (elem.checked) {
                var parts = label.innerHTML.split(',');
                openWindow.document.write("<table><tr><td>От кого: Администрация Вуза</td></tr><tr><td style = \"width:300px;\">Откуда: Ленина 54 , г Екатеринбург Свердловской обл</td></tr><tr><td>Индекс: 620000</td></tr></table>");
                openWindow.document.write("<div style=\"padding-left:20%;width:600px;\">" + message + "</div>");
                openWindow.document.write("<table style = \"padding-left:80%;\" align = \"right\">");
                openWindow.document.write("<tr><td>Кому:" + parts[1] + "</td></tr>");
                openWindow.document.write("<tr style =\"width:300px;\"><td>Адрес:" + parts[2] + "</td></tr>");
                openWindow.document.write("<tr><td>Индекс:" + parts[3] + "</td></tr>");
                openWindow.document.write("</table>");
            }
        }
    });
});

function copyInfo() {
    document.getElementById("MainContent_Login").value = document.getElementById("MainContent_LoginV").innerHTML;
    document.getElementById("MainContent_NameE").value = document.getElementById("MainContent_Name").innerHTML;
    document.getElementById("MainContent_SurnameE").value = document.getElementById("MainContent_Surname").innerHTML;
    document.getElementById("MainContent_SnameE").value = document.getElementById("MainContent_Sname").innerHTML;
    document.getElementById("MainContent_PSer").value = document.getElementById("MainContent_PSerV").innerHTML;
    document.getElementById("MainContent_PInd").value = document.getElementById("MainContent_PIndV").innerHTML;
    document.getElementById("MainContent_PDate").value = document.getElementById("MainContent_PDateV").innerHTML;
    document.getElementById("MainContent_RegionE").value = document.getElementById("MainContent_RegionT").innerHTML;
    document.getElementById("MainContent_RegionEH").value = document.getElementById("MainContent_RegionH").innerHTML;
    document.getElementById("MainContent_CityE").value = document.getElementById("MainContent_CityT").innerHTML;
    document.getElementById("MainContent_CityEH").value = document.getElementById("MainContent_CityH").innerHTML;
    document.getElementById("MainContent_StreetE").value = document.getElementById("MainContent_StreetT").innerHTML;
    document.getElementById("MainContent_StreetEH").value = document.getElementById("MainContent_StreetH").innerHTML;
    document.getElementById("MainContent_HouseE").value = document.getElementById("MainContent_HouseT").innerHTML;
    document.getElementById("MainContent_CorpE").value = document.getElementById("MainContent_CorpT").innerHTML;
    document.getElementById("MainContent_FlatE").value = document.getElementById("MainContent_FlatT").innerHTML;
    document.getElementById("MainContent_IndE").value = document.getElementById("MainContent_IndexT").innerHTML;
}