use NewKladr
GO

select * from George.adresses

if object_id('George.fio', 'U') is not null
	drop table George.fio
if object_id('George.accounts', 'U') is not null
	drop table George.accounts
if object_id('George.regions', 'U') is not null
	drop table George.regions
if object_id('George.districts', 'U') is not null
	drop table George.districts
if object_id('George.cities', 'U') is not null
	drop table George.cities
if object_id('George.reg_cities') is not null
	drop table George.reg_cities
if object_id('George.streets') is not null
	drop table George.streets	
if object_id('George.sub_cities') is not null
	drop table George.sub_cities	
if object_id('George.adresses') is not null
	drop table George.adresses
if object_id('George.codes') is not null
	drop table George.codes
if object_id('George.formCities') is not null
	drop procedure George.formCities
if object_id('George.formStreets') is not null
	drop procedure George.formStreets
go

IF EXISTS( SELECT * FROM sys.schemas WHERE name = N'George' ) 
DROP SCHEMA  George
GO
CREATE SCHEMA George
GO

create table George.adresses(
	id	int not null,
	region_name varchar(50) not null,
	region_help varchar(10) not null,
	city_name varchar(50) ,
	city_help varchar(50) ,
	street_name varchar(50),
	street_help varchar(50),
	house varchar(15) not null,
	corp varchar(10),
	flat varchar(10),
	post_index varchar(6) not null
)
go

create table George.accounts(
	id	int identity not null,
	user_login varchar(20) not null,
	user_password varchar (32) not null,
	is_superuser bit not null default 0,
	constraint PK_id primary key (id)
)
go

create table George.codes(
	id	int not null,
	city varchar(13) not null,
	street varchar (13) not null,
	house varchar(17),
)
go

create table George.fio(
	id	int ,
	name varchar(30) not null,
	surname varchar(30) not null,
	sname varchar(30) not null,
	PSer varchar(4) not null,
	PInd varchar(6) not null,
	PDate varchar(10) not null,
	constraint FK_id foreign key (id)
    references George.accounts(id)
)
go
create table George.cities(
	name varchar (40) not null,
	socr varchar (10) not null,
	code varchar (13) not null,
	par_region_name varchar (40),
	par_region_socr varchar (10),
	par_city_name varchar(40),
	par_city_socr varchar(10),
)
go
create table George.streets(
	name char (100) not null,
	socr varchar (10) not null,
	code varchar (17) not null,
	par_city_name varchar (40),
	par_city_socr varchar (10),
)
go

create procedure George.formCities as
declare @name varchar (40)
declare @socr varchar (10) 
declare @code varchar (17)
declare @parent_region_name varchar(40)
declare @parent_region_socr varchar(10)
declare @parent_city_name varchar(40)
declare @parent_city_socr varchar(10)
declare @region_id varchar(2)
declare @district_id varchar(3)
declare @city_id varchar(3)
declare @sub_city_id varchar(3)
declare @cursor cursor
set @cursor  = CURSOR scroll 
for
select name , socr , code from kladr where code not like '%00000000000' and code not like '%00000000' and socr not like '���'
open @cursor
fetch next from @cursor into @name , @socr , @code
while @@FETCH_STATUS = 0
begin
	set @region_id = SUBSTRING(@code,1,2)
	set @district_id = SUBSTRING(@code, 3 , 3)
	set @city_id = SUBSTRING(@code ,6,3)
	set @sub_city_id = SUBSTRING(@code,9,3)
	set @parent_city_name = null
	set @parent_city_socr = null
	set @parent_region_name = null
	set @parent_region_socr = null	
	if (@district_id like '000')
	begin
		if (@sub_city_id not like '000')	
		begin
			select @parent_city_name = name , @parent_city_socr = socr from George.reg_cities where code like @region_id+'000'+@city_id+'00000'
		end
	end
	else
	begin
		select @parent_region_name = name , @parent_region_socr = socr from George.districts where code like @region_id + @district_id + '%'		
		if (@sub_city_id not like '000' and @city_id not like '000')
		begin
			select @parent_city_name = name ,@parent_city_socr = socr from --kladr 
			George.sub_cities where code like @region_id+@district_id+@city_id+'00000'
		end
	end
	insert into George.cities values (@name,@socr,@code,@parent_region_name,@parent_region_socr,@parent_city_name,@parent_city_socr)
	fetch next from @cursor into @name , @socr , @code
end
close @cursor
go

create procedure George.formStreets as
declare @name char (100)
declare @socr varchar (10) 
declare @code varchar (17)
declare @parent_city_name varchar(40)
declare @parent_city_socr varchar(10)
declare @prefix_id varchar(11)
declare @parent_city_id varchar(3)
declare @cursor cursor
set @cursor  = CURSOR scroll 
for
select name , socr , code from street
open @cursor
fetch next from @cursor into @name , @socr , @code
while @@FETCH_STATUS = 0
begin
	set @prefix_id = SUBSTRING(@code , 1 , 11)
	set @parent_city_id = SUBSTRING(@code,9,3)
	set @parent_city_name = null
	set @parent_city_socr = null	
	if (@parent_city_id not like '000')
	begin
		select @parent_city_name = name , @parent_city_socr = socr	from George.sub_cities where code like @prefix_id+'%'
	end
	insert into George.streets values (@name,@socr,@code,@parent_city_name,@parent_city_socr)
	fetch next from @cursor into @name , @socr , @code
end
close @cursor
go

--Select all regions
select name , socr , code into George.regions from kladr where code like '%00000000000'
--Select all districts 
select name , socr , code into George.districts from kladr where code like '%[0-9][0-9][0-9]00000000' and code not like '%0000000000'
--Select all big cities
select name , socr , code into George.reg_cities from kladr where code like '%000[0-9][0-9][0-9]00000' and code not like '%00000000000'
--Select all sub sities
select name , socr , code into George.sub_cities from kladr where SUBSTRING(code,6,6) not like '000000'
--Select all cities with full addresses

exec George.formCities

--Select all streets with full addresses
--exec George.formStreets
insert into George.accounts (user_login,user_password,is_superuser) values ('admin','admin','1')

delete from George.fio where id = '3'
delete from George.adresses where id = '3'
delete from George.codes where id = '3'


select * from George.accounts
select * from George.fio
select * from George.codes
select * from George.adresses

select * from George.adresses as a cross join George.fio as b cross join George.accounts as c where a.id = b.id and b.id = c.id and c.is_superuser = '0'