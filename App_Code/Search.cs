﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Services;


/// <summary>
/// Summary description for Search
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]

public class Search : WebService
{
    private readonly int [] _odd = {1, 3, 5, 7, 9};
    private readonly int [] _even = {0, 2, 4, 6, 8};

    struct Addr
    {
        public readonly string Name;
        public readonly string Socr;
        public readonly string Code;
        public readonly string Ind;
        public Addr(string s, string socr1, string code1, string index)
        {
            Name = s;
            Socr = socr1;
            Code = code1;
            Ind = index;
        }
    } 
    
    [WebMethod]
    public string[] GetRegion ( string prefixText , int count )
    {
        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["KladrService"].ConnectionString))
        {
            conn.Open();
            var get = new SqlCommand("select * from George.regions where name like '" + prefixText + "%'", conn);
            var help = new List<string>();
            using (var reader = get.ExecuteReader())
            {
                if (reader.HasRows)
                    while (reader.Read())
                    {
                        var name = reader.GetValue(0).ToString().Trim();
                        var socr = reader.GetValue(1).ToString().Trim();
                        var code = reader.GetValue(2).ToString().Substring(0 , 2);
                        help.Add(name + " (" + socr + "):" + code);
                    }
                reader.Close();
            }
            conn.Close();            
            return help.ToArray();
        }
    }

    [WebMethod]
    public string[] GetCity ( string prefixText , int count , string contextKey )
    {
        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["KladrService"].ConnectionString))
        {
            conn.Open();
            var get = new SqlCommand("select * from George.cities where code like '" + contextKey + "%00'  and name like '" + prefixText + "%'" , conn);
            var cities = new List<string>();
            using (var reader = get.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var name = reader.GetValue(0).ToString().Trim();
                        var socr = reader.GetValue(1).ToString().Trim();
                        var code = reader.GetValue(2).ToString().Trim();
                        var rName = reader.GetValue(3).ToString().Trim();
                        var cName = reader.GetValue(5).ToString().Trim();
                        var sb = new StringBuilder();
                        sb.Append(socr + "," + name);
                        if (rName != "")
                            sb.Append("," + reader.GetValue(4).ToString().Trim() + "," + rName);
                        if (cName != "")
                            sb.Append("," + reader.GetValue(6).ToString().Trim() + "," + cName);
                        cities.Add(sb + ":" + code);
                    }
                }
                reader.Close();
            }
            conn.Close();
            return cities.ToArray();
        }        
    }

    [WebMethod]
    public string[] GetStreet ( string prefixText , int count , string contextKey )
    {
        var help = new List<string>();
        var streets = new List<Addr>();
        var counter = new Dictionary<string , int>();
        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["KladrService"].ConnectionString))
        {
            conn.Open();
            if (contextKey.Length == 2)
                contextKey += "000000000";
            var key = contextKey.Substring(0, 11);
            if (key.Substring(8 , 3) == "000")
                key = key.Substring(0 , 8);
            var get = new SqlCommand( "select name,socr,code,[index] from street where name like '" + prefixText + "%' and code like '" + key + "%'" , conn);
            using (var reader = get.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var name = reader.GetValue(0).ToString().Trim();
                        var socr = reader.GetValue(1).ToString().Trim();
                        var code = reader.GetValue(2).ToString().Trim();
                        var index = reader.GetValue(3).ToString().Trim();
                        streets.Add(new Addr(name , socr , code , index));
                    }
                    reader.Close();
                    foreach (Addr a in streets)
                    {
                        var helpmsg = a.Name + "," + a.Socr + "," + a.Code;
                        if (a.Code.Substring(8 , 3) != "000" && key.Length == 8)
                        {
                            var query = new SqlCommand( "select name , socr from George.cities where code like '" + a.Code.Substring(0 , 11) + "%'" , conn);

                            using (var r = query.ExecuteReader())
                            {
                                if (r.Read())
                                    helpmsg += "," + r.GetValue(1).ToString().Trim() + "," + r.GetValue(0).ToString().Trim();
                                r.Close();
                            }
                        }
                        helpmsg += ":" + a.Ind;
                        help.Add(helpmsg);
                    }
                }
            }
            conn.Close();
        }
        return help.ToArray();
    }

    [WebMethod]
    public string[] GetHouse ( string prefixText , int count , string contextKey )
    {
        if (contextKey == "none")
            return new string[]{};
        var help = new List<string>();
        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["KladrService"].ConnectionString))
        {
            conn.Open();
            var get = new SqlCommand("select name , [index] from doma where code like '" + contextKey + "%'", conn);
            using (var reader = get.ExecuteReader())
            {
                var patternO = new Regex("[Н()]");
                var patternE = new Regex("[Ч()]");
                if (reader.HasRows)
                    while (reader.Read())
                    {
                        string[] arr = reader.GetValue(0).ToString().Trim().Split(',');
                        var code = reader.GetValue(1).ToString().Trim();
                        foreach (string a in arr)
                        {
                            var firstChar = a.ElementAt(0);
                            if (firstChar == 'Н')
                            {
                                var result = patternO.Replace(a, "").Trim().Split('-');
                                var l = Convert.ToInt32(result[0]);
                                var r = Convert.ToInt32(result[1]);
                                int subi = 0;
                                if (prefixText != "")
                                    subi = Convert.ToInt32(prefixText);
                                foreach ( int temp in _odd.Select(q => subi*10 + q).Where( temp => temp <= r && temp >= l &&
                                            !help.Contains(temp.ToString() + ":" + code )))
                                    help.Add(temp + ":" + code);
                            }
                            else if (firstChar == 'Ч')
                            {
                                var result = patternE.Replace(a, "").Trim().Split('-');
                                var l = Convert.ToInt32(result[0]);
                                var r = Convert.ToInt32(result[1]);
                                int subi = 0;
                                if (prefixText != "")
                                    subi = Convert.ToInt32(prefixText);
                                foreach ( int temp in _even.Select(q => subi*10 + q).Where( temp => temp <= r && temp >= l &&
                                            !help.Contains(temp.ToString() + ":" + code)))
                                    help.Add(temp + ":" + code);
                            }
                            else if (a.Contains('-'))
                            {
                                var result = a.Trim().Split('-');
                                var l = Convert.ToInt32(result[0]);
                                var r = Convert.ToInt32(result[1]);
                                int subi = 0;
                                if (prefixText != "")
                                    subi = Convert.ToInt32(prefixText);
                                foreach (
                                    int temp in
                                        _even.Select(q => subi*10 + q).Where( temp => temp <= r && temp >= l &&
                                            !help.Contains(temp.ToString() + ":" + code)))
                                    help.Add(temp + ":" + code);
                            }
                            else if (prefixText.Length != 0)
                                if (a.Contains(prefixText))
                                    help.Add(a + ":" + code);
                        }
                    }
            }
            conn.Close();
        }
        return help.ToArray();
    }

    [WebMethod]
    public int GetCountStreets ( string code )
    {
        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["KladrService"].ConnectionString))
        {
            conn.Open();
            if (code.Length == 2)
                code += "000000000";
            var key = code.Substring(0, 11);
            if (key.Substring(8, 3) == "000")
                key = key.Substring(0, 8);
            var get = new SqlCommand("select count(*) from dbo.street where code like '" + key + "%'", conn);
            return (int) get.ExecuteScalar();
        }
    }
}
